def change(li):
    print(id(li)) # li 
    li[1] = li[1] + 2
    li = [3,3,3,4,5] # li 
    print(id(li))
li = [1,2,3,4,5]
print(id(li))
li = [3,3,3,4,5] 
print(id(li))

a = 5
print(id(a))
a = 10
print(id(a))

# python assigns the memory address by value as prefrense 
# not by value.